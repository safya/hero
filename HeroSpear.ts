import Hero from "./Hero";
import Weapon from "./Weapon";
import HeroAxe from "./HeroAxe";

export default class HeroSpear extends Hero{
    protected _weapon: Weapon;
    
    constructor(name: string, power: number, life: number, weapon: Weapon){
        super(name, power, life)
        this._weapon =  new Weapon("spear");
    }

    attack(opponent: HeroAxe){
        if(HeroAxe){
            return this._power * 2
        }
    }
}