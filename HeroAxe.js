"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Hero_1 = __importDefault(require("./Hero"));
const Weapon_1 = __importDefault(require("./Weapon"));
const HeroSword_1 = __importDefault(require("./HeroSword"));
class HeroAxe extends Hero_1.default {
    constructor(name, power, life) {
        super(name, power, life);
        this._weapon = new Weapon_1.default("axe");
    }
    attack(opponent) {
        if (opponent instanceof HeroSword_1.default) {
            opponent.life -= this._power * 2;
        }
        else {
            super.attack(opponent);
        }
    }
}
exports.default = HeroAxe;
