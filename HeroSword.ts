import Hero from "./Hero";
import Weapon from "./Weapon";
import HeroSpear from "./HeroSpear"

export default class HeroSword extends Hero{
    protected _weapon: Weapon;
    
    constructor(name: string, power: number, life: number,){
        super(name, power, life)
        this._weapon = new Weapon("sword");
    }

    attack(opponent: HeroSpear){
        if(HeroSpear){
            return this._power * 2
        }
    }
    
}