import Weapon from "./Weapon"

// Partie 1

export default class Hero{
    protected _name : string;
    protected _power : number;
    protected _life : number;
    protected _weapon? : Weapon;
    //weapon: Weapon;
    

    constructor(name: string, power: number, life: number){
        this._name = name;
        this._power = power;
        this._life = life;

    }

    get life(): number{
        return this._life
    }
    get name(): string{
        return this._name
    }

    set life(newLife: number){
        this._life = newLife;
    }

/* La méthode `attack` a un paramètre `opponent` (de type `Hero`). 
Il faut réduire le nombre (`life`) de `opponent` d'autant de dégats (`power`) de l'attaquant.
*/ 
    attack(opponent: Hero){
        opponent._life -= this._power;
    }


/*La méthode `isAlive` devrait retourner `true` 
si le nombre de points de vie du héros est supérieur à zéro et `false` sinon. */
    
    isAlive(): boolean{
        if(this._life > 0){
            return true
        }
        else{
            return false
        }
    }
}

let hero = new Hero("Safia", 200, 200)
let heracles = new Hero("Hercule", 100, 100)

hero.attack(heracles);
heracles.isAlive()

console.log(heracles.isAlive());
console.log(heracles);












