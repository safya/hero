"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Hero_1 = __importDefault(require("./Hero"));
const Weapon_1 = __importDefault(require("./Weapon"));
const HeroSpear_1 = __importDefault(require("./HeroSpear"));
class HeroSword extends Hero_1.default {
    constructor(name, power, life) {
        super(name, power, life);
        this._weapon = new Weapon_1.default("sword");
    }
    attack(opponent) {
        if (HeroSpear_1.default) {
            return this._power * 2;
        }
    }
}
exports.default = HeroSword;
