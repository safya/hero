"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Partie 1
class Hero {
    //weapon: Weapon;
    constructor(name, power, life) {
        this._name = name;
        this._power = power;
        this._life = life;
    }
    get life() {
        return this._life;
    }
    get name() {
        return this._name;
    }
    set life(newLife) {
        this._life = newLife;
    }
    /* La méthode `attack` a un paramètre `opponent` (de type `Hero`).
    Il faut réduire le nombre (`life`) de `opponent` d'autant de dégats (`power`) de l'attaquant.
    */
    attack(opponent) {
        opponent._life -= this._power;
    }
    /*La méthode `isAlive` devrait retourner `true`
    si le nombre de points de vie du héros est supérieur à zéro et `false` sinon. */
    isAlive() {
        if (this._life > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
exports.default = Hero;
let hero = new Hero("Safia", 200, 200);
let heracles = new Hero("Hercule", 100, 100);
hero.attack(heracles);
heracles.isAlive();
console.log(heracles.isAlive());
console.log(heracles);
