"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Weapon {
    constructor(name) {
        this._name = name;
    }
    get name() {
        return this._name;
    }
}
exports.default = Weapon;
