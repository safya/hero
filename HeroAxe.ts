import Hero from "./Hero"
import Weapon from "./Weapon";
import HeroSword from "./HeroSword"
export default class HeroAxe extends Hero{
    protected _weapon: Weapon;
    
    constructor(name: string, power: number, life: number){
        super(name, power, life);
        this._weapon = new Weapon("axe");
    }


    attack(opponent: HeroSword){
        if(opponent instanceof HeroSword){
            opponent.life -= this._power * 2
        }
        else{
            super.attack(opponent);
        }
    }

}

